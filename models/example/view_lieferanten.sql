
-- Welcome to your first dbt model!
-- Did you know that you can also configure models directly within
-- the SQL file? This will override configurations stated in dbt_project.yml

-- Try changing 'view' to 'table', then re-running dbt
{{ config(materialized='view') }}


SELECT CAST (tlieferant.nkreditorennr AS INT) AS Kreditor, tlieferant.cstatus AS Gruppe, tlieferant.cfirma FROM jtl.tlieferant
ORDER BY tlieferant.cstatus, tlieferant.nkreditorennr
