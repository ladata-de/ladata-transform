
-- Welcome to your first dbt model!
-- Did you know that you can also configure models directly within
-- the SQL file? This will override configurations stated in dbt_project.yml

-- Try changing 'view' to 'table', then re-running dbt
{{ config(materialized='view') }}


SELECT kbuchungsart, COUNT(kbuchungsart) FROM ladata.jtl.tartikelhistory GROUP BY kbuchungsart
