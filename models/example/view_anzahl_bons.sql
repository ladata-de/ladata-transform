
-- Welcome to your first dbt model!
-- Did you know that you can also configure models directly within
-- the SQL file? This will override configurations stated in dbt_project.yml

-- Try changing 'view' to 'table', then re-running dbt
{{ config(materialized='view') }}


SELECT COUNT(POS_Bon.cBonNr) AS Anzahl_Bons,
        date_part('month',POS_Bon.dDatum) AS Monat,
        date_part('year',POS_Bon.dDatum) AS Jahr
        FROM jtl.pos_bon
        GROUP BY date_part('month',POS_Bon.dDatum),
        date_part('year',POS_Bon.dDatum)
        ORDER BY Jahr, Monat
