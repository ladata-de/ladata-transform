
-- Welcome to your first dbt model!
-- Did you know that you can also configure models directly within
-- the SQL file? This will override configurations stated in dbt_project.yml

-- Try changing 'view' to 'table', then re-running dbt
{{ config(materialized='view') }}


SELECT ROUND(SUM(pos_bon.fGesamtSumme)) AS Umsatz,
       ROUND(SUM(pos_bon.fMwSt1)) AS Mwst19,
       ROUND(SUM(pos_bon.fMwSt2)) AS Mwst7,
       date_part('month',POS_Bon.dDatum) AS Monat,
       date_part('year',POS_Bon.dDatum) AS Jahr
       FROM jtl.pos_bon
       GROUP BY date_part('month',POS_Bon.dDatum),
       date_part('year',POS_Bon.dDatum)
       ORDER BY Jahr, Monat
